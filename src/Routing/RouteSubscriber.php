<?php

/**
 * @file
 * Contains \Drupal\wwu_disable_password_reset\Routing\RouteSubscruber.
 */

namespace Drupal\wwu_disable_password_reset\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('user.pass');

    if ($route) {
      $route->setRequirement('_access', 'FALSE');
    }
  }

}
